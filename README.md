# README.md 

# CPS 491 - Capstone II

Source: <https://bitbucket.org/capstones-cs-udayton/cps491.git>


University of Dayton

Department of Computer Science

CPS 491 - Capstone II, Fall Semester 2021

Instructor: Dr. Phu Phung


## Capstone II Project 


# AAJ Auto Group


# Team members

1.  Andre Cullen, cullena1@udayton.edu
2.  Anthony Avila, avilaa1@udayton.edu
3.  Johnny Ngo, ngoj01@udayton.edu
4.  Member 4, email


# Company Mentors

Mentor 1, Dr. Phu Phung; Mentor 2, _title_; ... 

AAJ Auto Group

Company address: 300 College Park, University of Dayton 


# Project Management Information

Management board (private access): https://trello.com/b/x52I3tta/sprint-management-aaj-cps-491

Source code repository (private access): https://bitbucket.org/cps491-f21-team2/aaj/src/master/

Project homepage (public): https://cps491-f21-team2.bitbucket.io/

## Revision History

| Date       |   Version     |  Description |
|------------|:-------------:|-------------:|
| 30/08/2021 |  0.0          |   Sprint 0   |
| 07/09/2021 |               |   Sprint 1   |


# Overview

Describe the overview of the project with a high-level architecture figure. 

During the recent Pandemic, we saw that the process of buying and researching the perfect car that you want is pretty difficult if you can't
be at the dealership in person to actually see all of the features of the car. This lead to us wanting to create a website that would make 
the whole car research and buying process a lot easier and contactless. With our website, AAJ Auto Group, you will be able to input your 
home location and we will show you a list of suggestions with the nearest car dealerships near you and a list of options that they offer. 
You can consider us as a middle man between you and the Car dealership that will try to enhance and simplify the entire car buying process. 

# Project Context and Scope

AAJ Auto Group's idea was created during Pandemic times where if someone wanted to go and purchase a vehicle, the process of doing so would have
been really hard due to the process we had to go through to actually go inside a store during pandemic times. The purpose of our project is that 
the our client does not have to leave the comfort of their own home to purchase, or rent a new vehicle. User will be able to input their location, 
and our website will show them the best dealerships around their area. Once the user has selected the car they want, there will be information 
where they can contact the dealership or they can continue the experience with us.

covid has made it so the user can not make it to the dealership so the focus of our project is to keep the user out of the shop. The times are tough so we dont want more people to get covid. we want to make the user feel safe and confident that they can go and buy a car without running the chance of getting sick. This is our main purpose to make the best option for the future to come 
 
_Note: For sprint 0, you can copy the content from your project proposal in CPS 490. You need to update this section according to in each sprint_ 

# System Analysis
Our website should be able to allow the user to input his/her location once the user enters the site. Once the user has entered their location, 
the website should redirect the user to a page with listings on all the dealerships that are close to the location he/she provided. Since our 
website is basically an ecommerce website, we will also implement a database for all of the dealerships we work with and the cars that they provide. 

## High-level Requirements

List high-level requirements of the project that your team will develop into use cases in later steps
User Inputs location: the user will have a text box to input there current location and the website will gather up all the near dealership so the user knows which is the best and nearest one fror there needs

website shows them dealerships near them. 

List of some popular cars per dealership. 
 once the user chooses the dealership that they the webiste will them a list of the popular cars they are offering.

Home Page: our home page will have the user input there location once they enter the website. also in the homepage we will have a step by step guide that shows the user how to use and navigate the website

Database: we need to create a database where we can store the dealerships in our service and all the cars in the dealership

## Use cases

Draw the overview use case diagram, and define use case description _(Main focus of Sprint 0)_

were working on rewriting these from last semester to make sure they fit for our new project.

### Use case 9
![Use case 9](https://trello.com/1/cards/613a355a3298497a761a2238/attachments/613a3598dd2fa36b03c6f3aa/download/Usecase9.png)

### use case 8
![Use case 8 ](https://trello.com/1/cards/613a355a3298497a761a2238/attachments/613a3596816e287a8c4a5cab/download/Usecase8.png)

### use case 7
![Use case 7](https://trello.com/1/cards/613a355a3298497a761a2238/attachments/613a35951ba4f6496fcc7a76/download/Usecase7.png)

### use case 6
![Use case 6](https://trello.com/1/cards/613a355a3298497a761a2238/attachments/613a35933acb710b422394b7/download/Usecase6.png)

### use case 5
![Use case 5](https://trello.com/1/cards/613a355a3298497a761a2238/attachments/613a3592147eca50cf3e7720/download/Usecase5.png)

### use case 4
![Use case 4](https://trello.com/1/cards/613a355a3298497a761a2238/attachments/613a35914620710391547a11/download/Usecase4.png)

### use case 3
![Use case 3](https://trello.com/1/cards/613a355a3298497a761a2238/attachments/613a358ffc2d780253b7bda3/download/Usecase3.png)

### use case 2

![Use case 2](https://trello.com/1/cards/613a355a3298497a761a2238/attachments/613a358ebe41df2b540a1ea1/download/Usecase2.png)

### use case 1
![Use case 1](https://trello.com/1/cards/613a355a3298497a761a2238/attachments/613a358d7ec2b45b2257c0dc/download/Usecase1.png)


# System Design

_(Start from Sprint 1, keep updating)_

## Use-Case Realization
Still working on uploading the images of our use cases but we have about 20 Use Cases: 
 User Inputs Location

 Servicing a car- 

 Browse Cars- done

 View Cars- done

 Purchase Car- 

 Contact Dealership of Choice- done

 Rent Car-

 Schedule Test Drive with Dealership-

 Contacting Website Support-

 Website Car Recommendations - done

## Database 
For our database we will be using MongoDB, our database will contain a list of dealerships, each car a dealership will contain, and lastly the users of the website.
Sprint 1-3: Created Database and connected with website Login 
Sprint 4-6: Connected Database with rest of website for Cars and Dealerships information. 
currently working to add the images to be pulled from the database.

### ER Diagram
![](https://i.imgur.com/gLctdDM.png)

Our current ER Diagram will be updating as needed.



### Sequence Diagram

images/cars.jpg



## User Interface
TBD, we are already working on the home page and user interface but is not yet ready to commit. 

# Implementation

_(Start from Sprint 1, keep updating. However, it is important to prepare the technology from Sprint 0)_

For each new sprint cycle, update the implementation of your system (break it down into subsections). It is helpful if you can include some code snippets to illustrate the implementation

Specify the development approach of your team, including programming languages, database, development, testing, and deployment environments. 
 in the works

## Deployment

Our project is an Online Dealership Service that will be deployed as a website using the bitbucket link: https://cps491-f21-team2.bitbucket.io/

# Software Process Management

_(Start from Sprint 0, keep updating)_

Our team is made up of a group of 3: Anthony Avila, Andre Cullen & Johnny Ngo. We have created a shared Trello board where we will keep all of our tasks up to date,
as well as divide the work between in progress and done as well as schedule the next and future tasks for the website. We are currently meeting three times a week
and using bitbucket to make sure we are all working with the same updated files and code. 

## Sprint Backlog
![](https://trello.com/1/cards/613a355a3298497a761a2238/attachments/613a3c93f80e734ba814ca05/download/Trello.png)


## Gantt Chart
![](https://trello.com/1/cards/613a355a3298497a761a2238/attachments/613a3c6c55d3b98e03c3eec6/download/GanttChart.png)

## Scrum process
We have divided our tasks in different Sprints where we establish a summary of the tasks we will have to complete by each Sprint. 

### Sprint 0

Duration: 24/08/2021-08/09/2021

#### Completed Tasks: 

1. README Update
2. Project Overview
3. Sprint Overviews 
4. Trello Board
5. Initial Commits

#### Contributions: 

1.  Andre Cullen, 1 commit (README), Trello Board Collaboration y hours, contributed in xxx
2.  Anthony Avila, Initial Commit, Trello Board Collaboration y hours, contributed in xxx
3.  Johnny Ngo, Initial Commit, Trello Board Collabortaion y hours, contributed in xxx
4.  Member 4, x commits, y hours, contributed in xxx

### Sprint 1

Duration: 08/31/2021-09/07/2021

#### Completed Tasks: 

1. Create Database
2. Finish Project Proposal
3. Start Working on Home Page to set a look and feel of the Website

#### Contributions: 

1.  Andre Cullen, 2 commits, 8 hours, contributed in developing the style and format of the website. 
2.  Anthony Avila, 1 commits, y hours, researched ways to code the website and did brainstroming with both parties. 
3.  Johnny Ngo, 1 commits, 4 hours, researched the database and was able to fins one that was usefull. 
4.  Member 4, x commits, y hours, contributed in xxx

#### Sprint Retrospection:

_(Introduction to Sprint Retrospection:

_Working through the sprints is a continuous improvement process. Discussing the sprint has just completed can improve the next sprints walk through a much efficient one. Sprint retrospection is done once a sprint is finished and the team is ready to start another sprint planning meeting. This discussion can take up to 1 hour depending on the ideal team size of 6 members. 
Discussing good things happened during the sprint can improve the team's morale, good team-collaboration, appreciating someone who did a fantastic job to solve a blocker issue, work well-organized, helping someone in need. This is to improve the team's confidence and keep them motivated.
As a team, we can discuss what has gone wrong during the sprint and come-up with improvement points for the next sprints. Few points can be like, need to manage time well, need to prioritize the tasks properly and finish a task in time, incorrect design lead to multiple reviews and that wasted time during the sprint, team meetings were too long which consumed most of the effective work hours. We can mention every problem is in the sprint which is hindering the progress.
Finally, this meeting should improve your next sprint drastically and understand the team dynamics well. Mention the bullet points and discuss how to solve it.)_

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|          |                             |                   |


### Sprint 2

Duration: 09/07/2021-09/13/2021

#### Completed Tasks: 

1. Website Car Recommendations
2. About Us Page
3. Contact Us Page
4. ER Diagram
5. Database Created
6. We researched and found the best way to implement the Locate Me search bar on the Home Page

#### Contributions: 

1.  Andre Cullen, 6commits, 8 hours, contributed in programming the Home Page, Website Car Recommendations, About Us, Contact Us and documentation. 
2.  Anthony Avila, 5 commits, 7hours, contributed by researching how to properly code the locate me button and started working on it's functionality. 
3.  Johnny Ngo, 3 commits, 7 hours, contributed in creating an ER diagram for our database and actually creating the database itself, also by creating the Login Page. 

#### Sprint Retrospection: 

| Great    |   finsihed our tasked great |  How to improve?                   |
|----------|:---------------------------:|----------------------:             |
|Team work |   Delegation of tasks       |More communication and finsih tasks |


# User guide/Demo

When you first come to out website you will be greated by our home page. once you get there you will be promted to enter your zipcode so that the website can access all the dealerships in the area you listed. you will then be taken to the dealership page where you will be able to chose the dealership that you want to buy from. once you click on the dealerships then you will be taken to the cars page where you will be able to see all the cars that are being sold at that the dealership.

# Acknowledgments 

You can thank the company that sponsors your project and individuals/organizations that supports your work.



print Retrospection:

_Working through the sprints is a continuous improvement process. Discussing the sprint has just completed can improve the next sprints walk through a much efficient one. Sprint retrospection is done once a sprint is finished and the team is ready to start another sprint planning meeting. This discussion can take up to 1 hour depending on the ideal team size of 6 members. 
Discussing good things happened during the sprint can improve the team's morale, good team-collaboration, appreciating someone who did a fantastic job to solve a blocker issue, work well-organized, helping someone in need. This is to improve the team's confidence and keep them motivated.
As a team, we can discuss what has gone wrong during the sprint and come-up with improvement points for the next sprints. Few points can be like, need to manage time well, need to prioritize the tasks properly and finish a task in time, incorrect design lead to multiple reviews and that wasted time during the sprint, team meetings were too long which consumed most of the effective work hours. We can mention every problem is in the sprint which is hindering the progress.
Finally, this meeting should improve your next sprint drastically and understand the team dynamics well. Mention the bullet points and discuss how to solve it.)_



### Sprint 3

Duration: 09/13/2021-09/20/2021

#### Completed Tasks: 

1. contact us and about us page
2. locate me Button Functionality
3. create database for deakerships,cars,and users for our login
4. login page
5. update user guide demo with screen shots

#### Contributions: 

1.  Andre Cullen, 5 commits, 9 hours, was able to get all the pages working and connected with html and css. 
2.  Anthony Avila, 3 commits, 8.5hours, figured out how to get the locate button to work. 
3.  Johnny Ngo, 4 commits, 8.5 hours, created the login page and connected to database. 


#### Sprint Retrospection:

_(Introduction to Sprint Retrospection:

_Working through the sprints is a continuous improvement process. Discussing the sprint has just completed can improve the next sprints walk through a much efficient one. Sprint retrospection is done once a sprint is finished and the team is ready to start another sprint planning meeting. This discussion can take up to 1 hour depending on the ideal team size of 6 members. 
Discussing good things happened during the sprint can improve the team's morale, good team-collaboration, appreciating someone who did a fantastic job to solve a blocker issue, work well-organized, helping someone in need. This is to improve the team's confidence and keep them motivated.
As a team, we can discuss what has gone wrong during the sprint and come-up with improvement points for the next sprints. Few points can be like, need to manage time well, need to prioritize the tasks properly and finish a task in time, incorrect design lead to multiple reviews and that wasted time during the sprint, team meetings were too long which consumed most of the effective work hours. We can mention every problem is in the sprint which is hindering the progress.
Finally, this meeting should improve your next sprint drastically and understand the team dynamics well. Mention the bullet points and discuss how to solve it.)_

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|          |                             |                   |
the work we put in was great we made better improvements then last sprint and got major progress done. we still need to imporve on some things like communicating a bit better but we are making good improvements on that at the moment.



Sprint 4

Duration: 09/20/2021-09/27/2021

#### Completed Tasks: 

1. cars page
2. locate me functionality
3. website footer
4. add to cart feature
5. 

#### Contributions: 

1.  Andre Cullen, 6commits, 8 hours, 
2.  Anthony Avila, 5commits, 7hours,  
3.  Johnny Ngo, 5commits, 8 hours, 




| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
| work     |  commits                    | work everyday     |


####Sprint Retrospection:

_this spring we focused a lot on doing the tasks we completed we ran into a of problems along the way which made us become delayed and push back a lot of our times.We were able to over come them and work as a team even though it took a lof of communcation in which we have not been good at throughout the project.we need to focus more on commits as we do too little.

Sprint 5

Duration: 09/28/2021-10/04/2021

#### Completed Tasks: 

1. Finished cars page
2. finish website footer
3. finished bitbucket homepage
4. implemented add to cart on all HTML pages
5. style dealerships

#### Contributions: 

1.  Andre Cullen, 8 commits, 11 hours, 
2.  Anthony Avila, 6 commits, 11.25 hours,  
3.  Johnny Ngo, 7commits, 12 hours, 




| Great    |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|progress  | commits                     |   work every day  |

####Sprint Retrospection:

this sprint was a lot of working on fixing up the overall website we spent the majority of the time doing small changes. we could have done more but those small changes made a big impact. we are still running into problems with the commits as we dont do too mcuh of them but hopefully we can fix those problems as we move on.

Sprint 6

Duration: 10/04/2021-10/13/2021

#### Completed Tasks: 

1. view cars page done for multiple dealerships
2. car page finished
3. updated footer on all html pages
4. add to cart icon on all html pages
5. database connected with website
6. entire website deployed

#### Contributions: 

1.  Andre Cullen, 9 commits, 9 hours, 
2.  Anthony Avila, 8commits, 8.5hours,  
3.  Johnny Ngo, 8 commits, 8.5 hours,  




| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|progress  |       communication         | start work early  |

Sprint Retrospection:

we were able to complete a lot of work this sprint. we connected everything and it is now live online and is connected to our database as well. we were able to move forward with a lot of the project and are making impacts in the project. we are still struggling to make commits but we are programming outside of project.

Sprint 7

Duration: 10/13/2021-10/20/2021

#### Sprint Backlog: 
1. Research how to retrieve images from dealership 
2. Create a Cart Page with Cart Icon on top right of all pages
3. Make Locate Me Button Functional (TOP PRIORITY)
4. Update Car Details page 


#### Completed Tasks: 

1. Found how to connect images to DB
One way we have found: 
		Input the same path of the image but directly into the MongoDB Database.
2. 

#### Contributions: 

1.  Andre Cullen, 2 commits, 3hours, 2 team collaboration hours.  
2.  Anthony Avila, 1commits, 2 hours, 2 team collaboration hours. 
3.  Johnny Ngo, 1 commits, 3 hours, 2 team collaboration hours. 




| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|progress  |       communication         | start work early  |

Sprint Retrospection:
So far we have met as a group to discuss what further tasks and features we would like to implement next in our project. We have decided that the next step should be to mainly focus on how to implement the locate me search bar so that when the user inputs their zip code it takes the user directly to the Dealerships page of the website with the different variety of dealership options that the user has around his/her area. Besides trying to make this feature functional, we want to focus on making sure the Car Details page so that it looks neater and so that it works for every car in our website. To start the last half of our project progress, we want to start implementing the Add to Cart functionality on all cars by creating a Cart page where that car option could be stored for the particular user. We know that for this, we would need to have id numbers for all users in our website as well as id numbers for all orders each user account processes. 

Worked on understanding and developing a strategy to implement Locate Me on Backend Side, connected to Test UI. We will have to populate the database with the different zip codes of the cities we want to be available on with the path to the certain dealerships that belong to that zip code in particular. We will send zipcodes through the frond end home page of the website to the back end and the back end will connect to the database of zip codes and retreieve all of the dealerships connected to that zip code and then retrieve it and show it on the dealerships front end page for the user to see the dealerships around the inputed zip code. 

Today Johnny and Andre met and tried to figure out what exact files needed to be edited in order to make this "Locate Me" feature work on our website. We identified some clear methods we had to use, like make requests to the backend to go to the database and check the list of zip codes and retrieve all of the dealerships on that list for that particular zip code. 

Sprint 8

Duration: 10/21/2021-10/27/2021

#### Sprint Backlog: 
1. Add Cars page to all dealerships with their respective images
2. Make the Add to Cart feature Functional
3. Make Locate Me Button Functional (TOP PRIORITY)

#### Completed Tasks: 

1. task completed locate me

#### Contributions: 

1.  Andre Cullen, 0 commits, 6 hours, 
2.  Anthony Avila, 0 commits, 5.5 hours,  
3.  Johnny Ngo, 0 commits, 8 hours,  




| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|progress  |       communication         | start work early  |

Sprint Retrospection:

after comleplting this sprint we as a group came together to discus what we have to do next as we are getting near the ending of the the project and the semster we. we got one of the hardest parts of the project completed it took us a good amount of time to get the locate me to finally start working as we were completly new to the way the function worked. we were able to learn alot from what we did in the locate me feature and how to we could implement it to other projects in the near future.

we figured out were we want to move on too with the project in the future. we are going to implement a few more updates. we are going to add a function that allows customers to make appoiments so they can go visist the dealerships


trying to figure out how to open mulitple sessions and researching 

