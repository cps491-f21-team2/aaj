const mongoose = require("mongoose");

const carsSchema = new mongoose.Schema({
  make: {
    type: String,
  },
  model: {
    type: String,
  },
  color: {
    type: String,
  },
  year: {
    type: String,
  },
  price: {
    type: String,
  },
  image: {
    type: String,
  },
});

const cars = mongoose.model("cars", carsSchema);

module.exports = cars;
