const mongoose = require("mongoose");

const dealershipsSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  address: {
    type: String,
  },
  city: {
    type: String,
  },
  state: {
    type: String,
  },
  zip: {
    type: String,
  },
  phone: {
    type: String,
  },
  image: {
    type: String,
  },
});

const dealerships = mongoose.model("dealerships", dealershipsSchema);

module.exports = dealerships;
