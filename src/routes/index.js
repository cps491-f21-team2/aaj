const express = require("express");
const router = express.Router();
const { ensureAuthenticated, forwardAuthenticated } = require("../config/auth");
const db = require("../config/keys").mongoURI;
var Cars = require("../models/Cars");
var Dealerships = require("../models/Dealerships");

// index
router.get("/", forwardAuthenticated, (req, res) => res.render("index"));

// Login
router.get("/login", forwardAuthenticated, (req, res) => res.render("login"));

// About Us
router.get("/AboutUs", forwardAuthenticated, (req, res) =>
  res.render("AboutUs")
);

// Car Details
router.get("/CarDetails", forwardAuthenticated, (req, res) =>
  res.render("CarDetails")
);

// Cart
router.get("/Cart", forwardAuthenticated, (req, res) =>
  res.render("Cart")
);

// Cars
router.get("/Cars", (req, res) => {
  Cars.find(function (err, car) {
    res.render("Cars", { cars: car });
  });
});

// Contact Us
router.get("/ContactUs", forwardAuthenticated, (req, res) =>
  res.render("ContactUs")
);

// Dealership
router.get("/Dealership", (req, res) => {
  Dealerships.find(function (err, dealership) {
    res.render("Dealership", { dealerships: dealership });
  });
});

// Dealership
router.get("/test/ziplocate", (req, res) => {
  //todo: 1.
  //get the zip code
  // var zip = document.getElementById("");
  //2. connect to the database
  var zip = req.query.zip;
  res.send("got zip:" + zip);
});

// test 2
router.get("/test", (req, res) => {
  Dealerships.find(function (err, dealership) {
    res.render("test", { dealerships: dealership });
  });
});

router.post("/test", function (req, res) {});
/* router.get("/test", (req, res) => {
  let zip = req.query.zip;
  let zip2 = new RegExp(zip, "i");
  if (zip != undefined) {
    let cursor = Dealerships.find({
      $or: [
        {
          zip: zip2,
        },
      ],
    });
    cursor.toArray(function (err, results) {
      console.log(results);
      res.send(results);
    });
  } else {
    let emptyInput = "Invalid Input";
    res.send(emptyInput);
  }
}); */

// Dashboard
router.get("/dashboard", ensureAuthenticated, (req, res) =>
  res.render("dashboard", {
    user: req.user,
  })
);

module.exports = router;
