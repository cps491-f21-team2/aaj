# CPS 491 - Capstone II
This file is automatically created by a script. Please delete this line and replace with the course and your team information.
## /style.css
```css
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
}
body{
	background-color: lightblue;
}
.navbar{
	display: flex;
	align-items: center;
	padding: 20px;

}
nav{
	flex: 1; 
	text-align: right;

}
nav ul{
	display: inline-block;
	list-style-type: none;
}
nav ul li{
	display: inline-block;
	margin-right: 20px;

}
a {
	text-decoration: none;
	color: #555;
}
p{
	color: #555;
}
.container{
	max-width: 1300px;
	margin: auto;
	padding-left: 25px;
	padding-right: 25px;
}
.row{
	display: flex;
	align-items: center;
	flex-wrap: wrap;
	justify-content: space-around;
}
.col-2 img{
	width: 100%;
	z-index: 0;
}
hr.dashed {
	border-top: 3px dashed #bbb;
}
.circle{
	display: block;
	margin-top: 100px;
	margin-left: 200px;
	margin-right: auto;
	border-radius: 50%;
	width: 34px;
	height: 34px;
	padding: 10px;
	background: yellow;
	border: 3px solid #000;
	color: #000;
	text-align: center;
	font: 32pxx Arial, sans-serif;
}
.step1{
	display: block;
	margin-top: 0px;
	margin-left: 230px;
	margin-right: auto;
}
.circle2{
	display: block;
	margin-top: 25px;
	margin-left: 200px;
	margin-right: auto;
	border-radius: 50%;
	width: 34px;
	height: 34px;
	padding: 10px;
	background: yellow;
	border: 3px solid #000;
	color: #000;
	text-align: center;
	font: 32pxx Arial, sans-serif;
}
.step2{
	display: block;
	margin-top: 0px;
	margin-left: 230px;
	margin-right: auto;
}
.circle3{
	display: block;
	margin-top: 0px;
	margin-left: 200px;
	margin-right: auto;
	border-radius: 50%;
	width: 34px;
	height: 34px;
	padding: 10px;
	background: yellow;
	border: 3px solid #000;
	color: #000;
	text-align: center;
	font: 32pxx Arial, sans-serif;
}
.step3{
	display: block;
	margin-top: 0px;
	margin-left: 230px;
	margin-right: auto;
}



















```
