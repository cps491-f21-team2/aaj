# CPS 491 - Capstone II
This file is automatically created by a script. Please delete this line and replace with the course and your team information.
## /index.html
```html
<!DOCTYPE html>
<html>
	<head>
			<meta charset="UTF-8">
			<title>AAJ Auto Group AA</title>
			<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>

		<div class="container">
			<div class="navbar">
			<div class="logo">
				<img src="images/logo.png" width="175px"><h1>AAJ Auto Group</h1>
			</div>
			<nav>
				<ul>
					<li><a href="">Home</a></li>
					<li><a href="">About Us</a></li>
					<li><a href="">Dealerships</a></li>
					<li><a href="">Cars</a></li>
					<li><a href="">Contact Us</a></li>
				</ul>

			</nav>
			</div>
			<div class="row">
				<div class="col-2">
					<h1>Input your Location, <br> and let Us take care of the rest!</h1>
					<form>
						<label for="Location">Input Your Location</label><br>
						<input type="text" id="Location" name="Location">
					</form>
					<p>We will find you the best dealerships around you <br>and show you a listing of their best vehicles.</p>

				</div>
				<div class="col-2">
					<img src="newcar.jpg">
				</div>
			</div>
			<hr class="dashed">
		</div>
		<div class="circle">1</div>
		<div class="step1"><h5>STEP 1:</h5></div>
		<div class="circle2">2</div>
		<div class="step2"><h5>STEP 2:</h5></div>
		<div class="circle3">3</div>
		<div class="step3"><h5>STEP 3:</h5></div>
		<!---Featured Cars--->
		<div class="categories">
				<div class="row">
					<div class="col-3">
						<img src="images/Nissan1.jpg">
					</div>
					<div class="col-3">
						<img src="images/CRV.png">
					</div>
					<div class="col-3">
						<img src="images/mazda.jpg">
					</div>

				</div>
		</div>

		
	</body>
</html>
```
## /ContactUs.html
```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
			<title>Contact Us</title>
			<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<div class="container">
			<div class="navbar">
			<div class="logo">
				<img src="images/logo.png" width="175px"><h1>AAJ Auto Group</h1>
			</div>
			<nav>
				<ul>
					<li><a href="index.html">Home</a></li>
					<li><a href="AboutUs.html">About Us</a></li>
					<li><a href="Dealership.html">Dealerships</a></li>
					<li><a href="">Cars</a></li>
					<li><a href="ContactUs.html">Contact Us</a></li>
				</ul>

			</nav>
			</div>
		</div>
		<div class="categories">
			<div class="small-container">
				<div class="row">
					<div class="col-3">
						<img src="images/Andre.png">
						<h4>André Cullen</h4>
						<p>Email: cullena1@udayton.edu</p>
					</div>
					<div class="col-3">
						<img src="images/Anthony.jpg">
						<h4>Anthony Avila</h4>
						<p>Email: avilaa1@udayton.edu</p>
					</div>
					<div class="col-3">
						<img src="images/Johnny.jpg">
						<h4>Johnny Ngo</h4>
						<p>ngoj01@udayton.edu</p>
					</div>
				</div>
			</div>
		</div>

	</body>
</html>
```
## /Dealership.html
```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
			<title>Dealerships</title>
			<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<div class="container">
			<div class="navbar">
			<div class="logo">
				<img src="images/logo.png" width="175px"><h1>AAJ Auto Group</h1>
			</div>
			<nav>
				<ul>
					<li><a href="index.html">Home</a></li>
					<li><a href="AboutUs.html">About Us</a></li>
					<li><a href="Dealerships.html">Dealerships</a></li>
					<li><a href="">Cars</a></li>
					<li><a href="ContactUs.html">Contact Us</a></li>
				</ul>

			</nav>
			</div>
		</div>
		<div class="banner">
			<h3>White Allen Chevrolet</h3>
			<p>Location: Dayton, Ohio</p>
			<p>Brand: Chevrolet </p>
		</div>
		<div class="banner">
			<h3>White Allen Honda</h3>
			<p>Location: Dayton, Ohio</p>
			<p>Brand: Honda</p>
		</div>
		<div class="banner">
			<h3>Express Motors</h3>
			<p>Location: Dayton, Ohio</p>
			<p>Brand: </p>
		</div>
		<div class="banner">
			<h3>Reichard Buick GMC</h3>
			<p>Location: Dayton, Ohio</p>
			<p>Brand: GMC </p>
		</div>
		<div class="banner">
			<h3>Select Auto</h3>
			<p>Location: Dayton, Ohio</p>
			<p>Brand: </p>
		</div>
		<div class="banner">
			<h3>SVG Motors</h3>
			<p>Location: Dayton, Ohio</p>
			<p>Brand: SVG</p>
		</div>
		<div class="banner">
			<h3>Krug Auto Sales</h3>
			<p>Location: Dayton, Ohio</p>
			<p>Brand: </p>
		</div>
		<div class="banner">
			<h3>Mercedes-Benz of Centerville</h3>
			<p>Location: Dayton, Ohio</p>
			<p>Brand: Mercedes-Benz </p>
		</div>
		<div class="banner">
			<h3>Bob Ross Auto Group</h3>
			<p>Location: Dayton, Ohio</p>
			<p>Brand: </p>
		</div>
		<div class="banner">
			<h3>White Allen Volkswagen</h3>
			<p>Location: Dayton, Ohio</p>
			<p>Brand: </p>
		</div>
		<div class="banner">
			<h3>Matt Castrucci Auto Mall</h3>
			<p>Location: Dayton, Ohio</p>
			<p>Brand: </p>
		</div>
		
	</body>
</html>
```
## /AboutUs.html
```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
			<title>About Us</title>
			<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<div class="container">
			<div class="navbar">
			<div class="logo">
				<img src="images/logo.png" width="175px"><h1>AAJ Auto Group</h1>
			</div>
			<nav>
				<ul>
					<li><a href="index.html">Home</a></li>
					<li><a href="AboutUs.html">About Us</a></li>
					<li><a href="Dealership.html">Dealerships</a></li>
					<li><a href="">Cars</a></li>
					<li><a href="ContactUs.html">Contact Us</a></li>
				</ul>

			</nav>
			</div>
		</div>
		<div class="categories">
			<div class="small-container">
				<div class="row">
					<div class="col-3">
						<img src="images/Andre.png">
						<h4>André Cullen</h4>
					</div>
					<div class="col-3">
						<img src="images/Anthony.jpg">
						<h4>Anthony Avila</h4>
					</div>
					<div class="col-3">
						<img src="images/Johnny.jpg">
						<h4>Johnny Ngo</h4>
					</div>
					<h2>André Cullen:</h2><p>Senior, at the University of Dayton studying Computer Information Systems with a minor in Business Administration. I have taken CPS 149, 150, 151, 242, 341, 350, 430, 470, and 471. Last summer I was a Digital Marketing Intern at Red Ventures in Charlotte, North Carolina. In the summer of 2019 I participated as an Intern in a local Start Up company in Puerto Rico, called Brands of Puerto Rico. I have skills in Java, HTML, CSS, and JavaScript.</p>
					

					<h2>Anthony Avila:</h2><p>Senior, at the University of Dayton studying Computer Information Systems with a minor in Economics. I have taken CPS 149, 150, 151, 242, 341, 350, and 430. Last summer I worked as an Intern at Chicago Harbor IT. I have skills in Java, python, and HTML. </p>

					<h2>Johnny Ngo:</h2><p>Senior, at the University of Dayton studying Computer Science. I have taken CPS 149, 150, 151, 250, 341 and 350. In the Summer of 2019, I worked as an Intern at LION, IT. I am proficient in Java, Python and C. </p>

				</div>
			</div>
		</div>		
	</body>
</html>
```
