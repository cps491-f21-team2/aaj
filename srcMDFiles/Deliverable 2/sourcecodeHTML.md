# CPS 491 - Capstone II
This file is automatically created by a script. Please delete this line and replace with the course and your team information.
## /views/CarDetails.ejs
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Car Details</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
  </head>
  <body>
    <div class="container">
      <div class="navbar">
        <div class="logo">
          <img src="images/logo.png" width="175px" />
          <h1>Specific Car</h1>
        </div>
        <nav>
          <ul>
            <li><a href="https://aaj-cps491.herokuapp.com/">Home</a></li>
            <li><a href="AboutUs">About Us</a></li>
            <li><a href="Dealership">Dealerships</a></li>
            <li><a href="Cars">Cars</a></li>
            <li><a href="ContactUs">Contact Us</a></li>
          </ul>
        </nav>
      </div>
    </div>
    <!-----------Single Product Details-------------->
    <div class="small-container">
      <div class="row">
        <div class="col-2">
          <img src="images/Nissan1.jpg" width="100%" id="product-img" />
          <div class="small-img-row">
            <div class="small-img-col">
              <img src="images/Nissan1.jpg" width="100%" />
            </div>
            <div class="small-img-col">
              <img src="images/Nissan1.jpg" width="100%" />
            </div>
            <div class="small-img-col">
              <img src="images/Nissan1.jpg" width="100%" />
            </div>
            <div class="small-img-col">
              <img src="images/Nissan1.jpg" width="100%" />
            </div>
          </div>
        </div>
        <div class="col-2">
          <p>Home / Cars</p>
          <h1>Nissan Rogue</h1>
          <h4>$20,000</h4>
          <select>
            <option>Select Model</option>
            <option>Sport</option>
            <option>Regular</option>
            <option>Upgraded</option>
          </select>
          <a href="" class="btn">Add to Cart</a>
          <h3>Product Details <i class="fas fa-indent"></i></h3>
          <br />
          <p>
            The Rogue is Nissan's best-selling vehicle, and it's also one of the
            most popular small SUVs on the market.
          </p>
        </div>
      </div>
    </div>
    <footer class="footer">
      <h3>Founders:</h3>
      <p>Anthony Avila: avilaa1@udayton.edu</p>
      <p>Andre Cullen: cullena1@udayton.edu</p>
      <p>Johnny Ngo: ngoj01@udayton.edu</p>
    </footer>
  </body>
</html>

```
## /views/test.ejs
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Cars</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
  </head>
  <body>
    <div class="container">
      <div class="navbar">
        <div class="logo">
          <img src="images/logo.png" width="175px" />
          <h1>Specific Dealership</h1>
        </div>
        <nav>
          <ul>
            <li><a href="index">Home</a></li>
            <li><a href="AboutUs">About Us</a></li>
            <li><a href="Dealership">Dealerships</a></li>
            <li><a href="Cars">Cars</a></li>
            <li><a href="ContactUs">Contact Us</a></li>
          </ul>
        </nav>
      </div>
    </div>
    <div class="products">
      <div class="containerP">
        <h1 class="page-title">ALL DEALERSHIPS</h1>
      </div>
      <div class="product-item">
        <% cars.forEach(function(car){ %>
        <!--Single Product-->
        <div class="product">
          <div class="product-content">
            <div class="product-img">
              <img src="images/Nissan1.jpg" alt="Product Image" />
            </div>
            <div class="product-btns">
              <button type="button" class="btn-cart">
                Add to Cart
                <span><i class="fas fa-plus"></i></span>
              </button>
              <button type="button" class="btn-buy">
                Buy Now
                <span><i class="fas fa-shopping-cart"></i></span>
              </button>
            </div>
          </div>
          <div class="product-info">
            <div class="product-info-top">
              <h2 class="car-title">
                <%=car.make%> <%=car.model%> (<%=car.year%>)
              </h2>
              <div class="rating">
                <span><i class="fas fa-star"></i></span>
                <span><i class="fas fa-star"></i></span>
                <span><i class="fas fa-star"></i></span>
                <span><i class="fas fa-star"></i></span>
                <span><i class="far fa-star"></i></span>
              </div>
            </div>
            <a href="#" class="product-name"><%=car.make%> <%=car.model%></a>
            <p class="product-price">$<%=car.price%></p>
          </div>
        </div>
        <!--Single Product end-->
      </div>
    </div>
    <% }); %>
    <footer class="footer">
      <h3>Founders:</h3>
      <p>Anthony Avila: avilaa1@udayton.edu</p>
      <p>Andre Cullen: cullena1@udayton.edu</p>
      <p>Johnny Ngo: ngoj01@udayton.edu</p>
    </footer>
  </body>
</html>

```
## /views/layout.ejs
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
      integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
      crossorigin="anonymous"
    />
    <link
      rel="stylesheet"
      href="https://bootswatch.com/5/cerulean/bootstrap.min.css"
    />
    <title>AAJ Auto Group</title>
  </head>
  <body>
    <div class="container"><%- body %></div>

    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
  </body>
</html>

```
## /views/index.ejs
```html
<!DOCTYPE html>
<html>
	<head>
			<meta charset="UTF-8">
			<title>AAJ Auto Group AA</title>
			<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>

		<div class="container">
			<div class="navbar">
			<div class="logo">
				<img src="images/logo.png" width="175px"><h1>AAJ Auto Group</h1>
			</div>
			<input type="button" onclick="location.href='login';" value="Login" />
			<nav>
				<ul>
					<li><a href="https://aaj-cps491.herokuapp.com/">Home</a></li>
					<li><a href="AboutUs">About Us</a></li>
					<li><a href="Dealership">Dealerships</a></li>
					<li><a href="Cars">Cars</a></li>
					<li><a href="ContactUs">Contact Us</a></li>
				</ul>
			</nav>
			</div>
			<div class="row">
				<div class="col-2">
					<h1>Input your Location, <br> and let Us take care of the rest!</h1>
					<form>
						<label for="Location">Input Your Location</label><br>
						<input type="text" id="Location" name="Location">
					</form>
					<p>We will find you the best dealerships around you <br>and show you a listing of their best vehicles.</p>

				</div>
				<div class="col-2">
					<img src="images/newcar.jpg">
				</div>
			</div>
			<hr class="dashed">
		</div>
		<div class="circle">1</div>
		<div class="step1"><h5>STEP 1:</h5><p>Input your <h5>ZIP CODE</h5> and let us find your nearest dealerships</p></div>
		<div class="circle2">2</div>
		<div class="step2"><h5>STEP 2:</h5><p>Browse around the multiple dealership options and choose your favorite one. </p></div>
		<div class="circle3">3</div>
		<div class="step3"><h5>STEP 3:</h5><p>Browse and Choose your favorite car and add it to your cart. </p></div>
		<!---Featured Cars--->
		<h1>_____________________________________Popular Options______________________________________</h1>
		<div class="categories">
			<div class="small-container">
				<div class="row">
					<div class="col-3">
						<img src="images/Nissan1.jpg">
						<h4>Nissan Rogue 2021</h4>
					</div>
					<div class="col-3">
						<img src="images/CRV.png">
						<h4>Honda CRV 2021</h4>
					</div>
					<div class="col-3">
						<img src="images/mazda.jpg">
						<h4>Mazda CX-5 2021</h4>
					</div>

				</div>
			</div>
		</div>
		<footer class="footer">
			<h3>Founders: </h3>
			<p>Anthony Avila: avilaa1@udayton.edu</p>
			<p>Andre Cullen: cullena1@udayton.edu</p>
			<p>Johnny Ngo: ngoj01@udayton.edu</p>
		</footer>		
	</body>
</html>
```
## /views/Dealership.ejs
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Dealerships</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
  </head>
  <body>
    <div class="container">
      <div class="navbar">
        <div class="logo">
          <img src="images/logo.png" width="175px" />
          <h1>AAJ Auto Group</h1>
        </div>
        <nav>
          <ul>
            <li><a href="https://aaj-cps491.herokuapp.com/">Home</a></li>
            <li><a href="AboutUs">About Us</a></li>
            <li><a href="Dealership">Dealerships</a></li>
            <li><a href="Cars">Cars</a></li>
            <li><a href="ContactUs">Contact Us</a></li>
          </ul>
        </nav>
      </div>
    </div>
    <% dealerships.forEach(function(dealership){ %>
    <div class="banner">
      <img src="images/WhiteAllenChevy.jpeg" />
      <h3><%=dealership.name%></h3>
      <p>Location: <%=dealership.city%>, <%=dealership.state%></p>
      <p>Phone: <%=dealership.phone%></p>
    </div>
    <% }); %>
    <footer class="footer">
      <h3>Founders:</h3>
      <p>Anthony Avila: avilaa1@udayton.edu</p>
      <p>Andre Cullen: cullena1@udayton.edu</p>
      <p>Johnny Ngo: ngoj01@udayton.edu</p>
    </footer>
  </body>
</html>

```
## /views/dashboard.ejs
```html
<h1 class="mt-4">Dashboard</h1>
<p class="lead mb-3">Welcome <%= user.name %></p>
<a href="/users/logout" class="btn btn-secondary">Logout</a>

```
## /views/ContactUs.ejs
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Contact Us</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
  </head>
  <body>
    <div class="container">
      <div class="navbar">
        <div class="logo">
          <img src="images/logo.png" width="175px" />
          <h1>AAJ Auto Group</h1>
        </div>
        <nav>
          <ul>
            <li><a href="https://aaj-cps491.herokuapp.com/">Home</a></li>
            <li><a href="AboutUs">About Us</a></li>
            <li><a href="Dealership">Dealerships</a></li>
            <li><a href="Cars">Cars</a></li>
            <li><a href="ContactUs">Contact Us</a></li>
          </ul>
        </nav>
      </div>
    </div>
    <div class="categories">
      <div class="small-container">
        <div class="row">
          <div class="col-3">
            <img src="images/Andre.png" />
            <h4>André Cullen</h4>
            <p>Email: cullena1@udayton.edu</p>
          </div>
          <div class="col-3">
            <img src="images/Anthony.jpg" />
            <h4>Anthony Avila</h4>
            <p>Email: avilaa1@udayton.edu</p>
          </div>
          <div class="col-3">
            <img src="images/Johnny.jpg" />
            <h4>Johnny Ngo</h4>
            <p>ngoj01@udayton.edu</p>
          </div>
        </div>
      </div>
    </div>
    <footer class="footer">
      <h3>Founders:</h3>
      <p>Anthony Avila: avilaa1@udayton.edu</p>
      <p>Andre Cullen: cullena1@udayton.edu</p>
      <p>Johnny Ngo: ngoj01@udayton.edu</p>
    </footer>
  </body>
</html>

```
## /views/Cars.ejs
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Cars</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
  </head>
  <body>
    <div class="container">
      <div class="navbar">
        <div class="logo">
          <img src="images/logo.png" width="175px" />
          <h1>Specific Dealership</h1>
        </div>
        <nav>
          <ul>
            <li><a href="https://aaj-cps491.herokuapp.com/">Home</a></li>
            <li><a href="AboutUs">About Us</a></li>
            <li><a href="Dealership">Dealerships</a></li>
            <li><a href="Cars">Cars</a></li>
            <li><a href="ContactUs">Contact Us</a></li>
          </ul>
        </nav>
      </div>
    </div>
    <div class="products">
      <div class="containerP">
        <h1 class="page-title">ALL DEALERSHIPS</h1>
      </div>
      <% cars.forEach(function(car){ %>
      <div class="product-item">
        <!--Single Product-->
        <div class="product">
          <div class="product-content">
            <div class="product-img">
              <img src="images/Nissan1.jpg" alt="Product Image" />
            </div>
            <div class="product-btns">
              <button type="button" class="btn-cart">
                Add to Cart
                <span><i class="fas fa-plus"></i></span>
              </button>
              <button type="button" class="btn-buy">
                Buy Now
                <span><i class="fas fa-shopping-cart"></i></span>
              </button>
            </div>
          </div>
          <div class="product-info">
            <div class="product-info-top">
              <h2 class="car-title">
                <a href="CarDetails"><%=car.make%> <%=car.model%></a>
              </h2>
              <div class="rating">
                <span><i class="fas fa-star"></i></span>
                <span><i class="fas fa-star"></i></span>
                <span><i class="fas fa-star"></i></span>
                <span><i class="fas fa-star"></i></span>
                <span><i class="far fa-star"></i></span>
              </div>
            </div>
            <a href="#" class="product-name"><%=car.make%> <%=car.model%></a>
            <p class="product-price">$<%=car.price%></p>
          </div>
        </div>
        <!--Single Product end-->
      </div>
      <% }); %>
    </div>
    <footer class="footer">
      <h3>Founders:</h3>
      <p>Anthony Avila: avilaa1@udayton.edu</p>
      <p>Andre Cullen: cullena1@udayton.edu</p>
      <p>Johnny Ngo: ngoj01@udayton.edu</p>
    </footer>
  </body>
</html>

```
## /views/register.ejs
```html
<div class="row mt-5">
  <div class="col-md-6 m-auto">
    <div class="card card-body">
      <h1 class="text-center mb-3">
        <i class="fas fa-user-plus"></i> Register
      </h1>
      <% include ("./partials/messages") %>
      <form action="/users/register" method="POST">
        <div class="form-group">
          <label for="name">Name</label>
          <input
            type="name"
            id="name"
            name="name"
            class="form-control"
            placeholder="Enter Name"
            value="<%= typeof name != 'undefined' ? name : '' %>"
          />
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            class="form-control"
            placeholder="Enter Email"
            value="<%= typeof email != 'undefined' ? email : '' %>"
          />
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input
            type="password"
            id="password"
            name="password"
            class="form-control"
            placeholder="Create Password"
            value="<%= typeof password != 'undefined' ? password : '' %>"
          />
        </div>
        <div class="form-group">
          <label for="password2">Confirm Password</label>
          <input
            type="password"
            id="password2"
            name="password2"
            class="form-control"
            placeholder="Confirm Password"
            value="<%= typeof password2 != 'undefined' ? password2 : '' %>"
          />
        </div>
        <button type="submit" class="btn btn-primary btn-block">
          Register
        </button>
      </form>
      <p class="lead mt-4">Have An Account? <a href="/users/login">Login</a></p>
    </div>
  </div>
</div>

```
## /views/AboutUs.ejs
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>About Us</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
  </head>
  <body>
    <div class="container">
      <div class="navbar">
        <div class="logo">
          <img src="/images/logo.png" width="175px" />
          <h1>AAJ Auto Group</h1>
        </div>
        <nav>
          <ul>
            <li><a href="https://aaj-cps491.herokuapp.com/">Home</a></li>
            <li><a href="AboutUs">About Us</a></li>
            <li><a href="Dealership">Dealerships</a></li>
            <li><a href="Cars">Cars</a></li>
            <li><a href="ContactUs">Contact Us</a></li>
          </ul>
        </nav>
      </div>
    </div>
    <div class="categories">
      <div class="small-container">
        <div class="row">
          <div class="col-3">
            <img src="/images/Andre.png" />
            <h4>André Cullen</h4>
          </div>
          <div class="col-3">
            <img src="/images/Anthony.jpg" />
            <h4>Anthony Avila</h4>
          </div>
          <div class="col-3">
            <img src="/images/Johnny.jpg" />
            <h4>Johnny Ngo</h4>
          </div>
          <h2>André Cullen:</h2>
          <p>
            Senior, at the University of Dayton studying Computer Information
            Systems with a minor in Business Administration. I have taken CPS
            149, 150, 151, 242, 341, 350, 430, 470, and 471. Last summer I was a
            Digital Marketing Intern at Red Ventures in Charlotte, North
            Carolina. In the summer of 2019 I participated as an Intern in a
            local Start Up company in Puerto Rico, called Brands of Puerto Rico.
            I have skills in Java, HTML, CSS, and JavaScript.
          </p>

          <h2>Anthony Avila:</h2>
          <p>
            Senior, at the University of Dayton studying Computer Information
            Systems with a minor in Economics. I have taken CPS 149, 150, 151,
            242, 341, 350, and 430. Last summer I worked as an Intern at Chicago
            Harbor IT. I have skills in Java, python, and HTML.
          </p>

          <h2>Johnny Ngo:</h2>
          <p>
            Senior, at the University of Dayton studying Computer Science. I
            have taken CPS 149, 150, 151, 250, 341 and 350. In the Summer of
            2019, I worked as an Intern at LION, IT. I am proficient in Java,
            Python and C.
          </p>
        </div>
      </div>
    </div>
    <footer class="footer">
      <h3>Founders:</h3>
      <p>Anthony Avila: avilaa1@udayton.edu</p>
      <p>Andre Cullen: cullena1@udayton.edu</p>
      <p>Johnny Ngo: ngoj01@udayton.edu</p>
    </footer>
  </body>
</html>

```
## /views/login.ejs
```html
<div class="row mt-5">
  <div class="col-md-6 m-auto">
    <div class="card card-body">
      <h1 class="text-center mb-3"><i class="fas fa-sign-in-alt"></i> Login</h1>
      <% include ("./partials/messages") %>
      <form action="/users/login" method="POST">
        <div class="form-group">
          <label for="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            class="form-control"
            placeholder="Enter Email"
          />
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input
            type="password"
            id="password"
            name="password"
            class="form-control"
            placeholder="Enter Password"
          />
        </div>
        <button type="submit" class="btn btn-primary btn-block">Login</button>
      </form>
      <p class="lead mt-4">
        No Account? <a href="/users/register">Register</a>
      </p>
    </div>
  </div>
</div>

```
## /views/welcome.ejs
```html
<div class="row mt-5">
  <div class="col-md-6 m-auto">
    <div class="card card-body text-center">
      <h1><img src="https://bitbucket.org/cps491-f21-team2/aaj/raw/b43c9aca8629f637820669c3a0e17b40ffc8e441/logo.png"></h1>
      <p>Create an account or login</p>
      <a href="/users/register" class="btn btn-primary btn-block mb-2"
        >Register</a
      >
      <a href="/users/login" class="btn btn-secondary btn-block">Login</a>
    </div>
  </div>
</div>

```
## /views/partials/messages.ejs
```html
<% if(locals.errors){ %> <% errors.forEach(function(error) { %>
<div class="alert alert-warning alert-dismissible fade show" role="alert">
  <%= error.msg %>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<% }); %> <% } %> <% if(success_msg != ''){ %>
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <%= success_msg %>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<% } %> <% if(error_msg != ''){ %>
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <%= error_msg %>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<% } %> <% if(error != ''){ %>
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <%= error %>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<% } %>

```
## /views/projectHomepage.ejs
```html
<!DOCTYPE html >
<html lang="en"><head>
    </head>
        <body>
            <h1 style="padding-left: 400px; height: 60px; font-size: 60px; border-width: 0; color: burlywood"> AAJ by Andre,Anthony,and Johnny</h1>
            <br>
        
            <h4 style="font-size: 40px; color: black">Our Team
            </h4>
            <h5 style="font-size: 30px; color: black">Andre Cullen</h5><br>
            <img src="" alt="Image of Andre" style="height:600px;">
            <br>
            <h6 style="font-size: 30px; color: black">Anthony Avila</h6>
            <img src="" alt="Image of Anthony" style="height:600px;"><br>
            <br>
			<br>
            <h6 style="font-size: 30px; color: black">Johnnny Ngo</h6>
            <img src="" alt="Image of Johnny" style="height:600px;"><br>
            <br>
            <br>
            <h7 style="font-size: 40px; color: black"> About the Project</h7>
            <p style="font-size: 20px;">
                The goal of this project is to develop a website that allows the customer to buy a car 100% online
            </p>
            <h7 style="font-size: 40px; color: black"> Project Description</h7>
            <p style="font-size: 20px;">
                The purpose of the project is to develop a website,that allows people to change the way they buy cars. we want people to come to our webiste before they even think about going to a in-person dealership .</p>   
            <h8 style="font-size: 40px; color: black"> </h8>
            <p style="font-size: 20px;"> </p>        
    </body></html>
```
