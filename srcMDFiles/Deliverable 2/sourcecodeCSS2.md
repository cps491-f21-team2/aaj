# CPS 491 - Capstone II
This file is automatically created by a script. Please delete this line and replace with the course and your team information.
## /style.css
```css
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
}
body{
	background-color: lightblue;
}
.navbar{
	display: flex;
	align-items: center;
	padding: 20px;

}
nav{
	flex: 1; 
	text-align: right;

}
nav ul{
	display: inline-block;
	list-style-type: none;
}
nav ul li{
	display: inline-block;
	margin-right: 20px;

}
a { 
	text-decoration: none;
	color: #555;
}
p{
	color: #555;
}
.container{
	max-width: 1300px;
	margin: auto;
	padding-left: 25px;
	padding-right: 25px;
}
.row{
	display: flex;
	align-items: center;
	flex-wrap: wrap;
	justify-content: space-around;
}
.col-2 img{
	width: 100%;
	z-index: 0;
}

hr.dashed {
	border-top: 3px dashed #bbb;
}
.circle{
	display: block;
	margin-top: 100px;
	margin-left: 200px;
	margin-right: auto;
	border-radius: 50%;
	width: 34px;
	height: 34px;
	padding: 10px;
	background: yellow;
	border: 3px solid #000;
	color: #000;
	text-align: center;
	font: 32pxx Arial, sans-serif;
}
.step1{
	display: block;
	margin-top: 0px;
	margin-left: 230px;
	margin-right: auto;
}
.circle2{
	display: block;
	margin-top: 25px;
	margin-left: 200px;
	margin-right: auto;
	border-radius: 50%;
	width: 34px;
	height: 34px;
	padding: 10px;
	background: yellow;
	border: 3px solid #000;
	color: #000;
	text-align: center;
	font: 32pxx Arial, sans-serif;
}
.step2{
	display: block;
	margin-top: 0px;
	margin-left: 230px;
	margin-right: auto;
}
.circle3{
	display: block;
	margin-top: 0px;
	margin-left: 200px;
	margin-right: auto;
	border-radius: 50%;
	width: 34px;
	height: 34px;
	padding: 10px;
	background: yellow;
	border: 3px solid #000;
	color: #000;
	text-align: center;
	font: 32pxx Arial, sans-serif;
}
.step3{
	display: block;
	margin-top: 0px;
	margin-left: 230px;
	margin-right: auto;
}
.col-3 img{
	width: 300px;
	z-index: 0;
}
.footer {
	background-color: black;
	padding-top: 25px;
	padding-bottom: 25px;
}
.footer p{
	color: white;
	padding-top: 25px;
}
.footer h3{
	color: lightblue;
}
.banner {
border-style: solid;

}
.banner h3{

}
.banner p{
	padding-top: 10px ;
	padding-bottom: 10px;
	padding-left: 10px;
	padding-right:10px ;

}
.banner {
	padding-top: 20px;
	padding-bottom: 20px;
	padding-left:  30px;
	padding-right: 30px;
	border: 1px solid ;
	background-color: white;
}
.banner img{
	width: 100%;
	display: block;
}
.product-img img{
	width: 100%;
	display: block;
}
.containerP {
	width: 88vw;
	margin: 0 auto;
	border-style: solid;
}
.page-title{
	font-size: 2.5rem;
	font-weight: 500;
	text-align: center;
	padding: 1.3rem 0;
	opacity: 0.9;
}
.car-title {
	padding: 0.6rem 0;
	text-transform: capitalize;
}
.products{
	background: var(--alice-blue);
	padding: 3.2rem 0;
}
.product{
	margin: 2rem;
	position: relative;
	border-style: solid;

}
.product-content{
	background: var(--gray);
	padding: 3rem 0.5rem 2rem 0.5rem;
	cursor: pointer;
	
}
.product-img{
	background: var(--white-light);
	box-shadow: 0 0 20px 10px var(--white-light);
	width: 200px;
	height: 200px;
	margin: 0 auto;
	border-radius: 50%;
	transition: background 0.5s ease;
}
.product-btns{
	display: flex;
	justify-content: center;
	margin-top: 1.4rem;
	opacity: 1;
	transition opacity 0.6s ease;
}
.btn-cart, .btn-buy{
	background: transparent;
	border: 1px solid black;
	padding: 0.8rem 0;
	width: 125px;
	font-family: inherit;
	text-transform: uppercase;
	cursor: pointer;
	border: none;
	transition: all 0.6s ease;
}
.btn-cart{
	background: black;
	color: white;
}
.btn-cart:hover{
	background: var(--carribean-green);
}
.btn-buy {
	background: white;
}
.btn-buy:hover{
	background: var(--carribean-green);
	color: #fff;
}
.product-info {
	background: white;
	padding: 2rem;

}
.product-info-top{
	display: flex;
	justify-content: space-between;
	align-items: center;
}
.rating span{
	color: black;
}
.product-name{
	color: black;
	display: block;
	text-decoration: none;
	font-size: 1rem;
	text-transform: uppercase;
	font-weight: bold;
}
.product-price{
	padding-top: 0.6rem;
	padding-right: 0.6rem;
	display:inline-block;
}
.product-img img{
	transition: transform 0.6s ease;
}
.product:hover .product-img img{
	transform:scale(1,1);
}

.product-items{
	display: grid;
	grid-template-columns: repeat(2,1fr);
}

.product-items{
	display: grid;
	grid-template-columns: repeat(3,1fr);
}
.product{
	margin-right: 1rem;
	margin-left: 1rem;
}
/*------Single Product Details------*/

.small-container{
	margin-top: 80px;
}
.small-container .col-2 img{
	padding: 0;

}
.small-container .col-2{
	padding: 20px;
}
.small-container h4{
	margin: 20px 0;
	font-size: 22px;
	font-weight: bold;
}
.small-container select{
	display: block;
	padding: 10px;
	margin-top: 20px;
}
.small-container .fa{
	color: #ff523b;
}
.small-img-row {
	display: flex;
	justify-content: space-between;
}
.small-img-col{
	flex-basis: 24%;
	cursor: pointer;
}





































```
