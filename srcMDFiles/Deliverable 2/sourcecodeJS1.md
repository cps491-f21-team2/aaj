# CPS 491 - Capstone II
This file is automatically created by a script. Please delete this line and replace with the course and your team information.
## /routes/users.js
```nodejs
const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const passport = require("passport");
// Load User model
const User = require("../models/User");
const { forwardAuthenticated } = require("../config/auth");

// Login Page
router.get("/login", forwardAuthenticated, (req, res) => res.render("login"));

// Register Page
router.get("/register", forwardAuthenticated, (req, res) =>
  res.render("register")
);

// Register
router.post("/register", (req, res) => {
  const { name, email, password, password2 } = req.body;
  let errors = [];

  if (!name || !email || !password || !password2) {
    errors.push({ msg: "Please enter all fields" });
  }

  if (password != password2) {
    errors.push({ msg: "Passwords do not match" });
  }

  if (password.length < 6) {
    errors.push({ msg: "Password must be at least 6 characters" });
  }

  if (errors.length > 0) {
    res.render("register", {
      errors,
      name,
      email,
      password,
      password2,
    });
  } else {
    User.findOne({ email: email }).then((user) => {
      if (user) {
        errors.push({ msg: "Email already exists" });
        res.render("register", {
          errors,
          name,
          email,
          password,
          password2,
        });
      } else {
        const newUser = new User({
          name,
          email,
          password,
        });

        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser
              .save()
              .then((user) => {
                req.flash(
                  "success_msg",
                  "You are now registered and can log in"
                );
                res.redirect("/users/login");
              })
              .catch((err) => console.log(err));
          });
        });
      }
    });
  }
});

// Login
router.post("/login", (req, res, next) => {
  passport.authenticate("local", {
    successRedirect: "https://cps491-f21-team2.bitbucket.io/",
    failureRedirect: "/users/login",
    failureFlash: true,
  })(req, res, next);
});

// Logout
router.get("/logout", (req, res) => {
  req.logout();
  req.flash("success_msg", "You are logged out");
  res.redirect("/users/login");
});

module.exports = router;

```
## /routes/index.js
```nodejs
const express = require("express");
const router = express.Router();
const { ensureAuthenticated, forwardAuthenticated } = require("../config/auth");
const db = require("../config/keys").mongoURI;
var Cars = require("../models/Cars");
var Dealerships = require("../models/Dealerships");

// index
router.get("/", forwardAuthenticated, (req, res) => res.render("index"));

// Login
router.get("/login", forwardAuthenticated, (req, res) => res.render("login"));

// About Us
router.get("/AboutUs", forwardAuthenticated, (req, res) =>
  res.render("AboutUs")
);

// Car Details
router.get("/CarDetails", forwardAuthenticated, (req, res) =>
  res.render("CarDetails")
);

// Cars
router.get("/Cars", (req, res) => {
  Cars.find(function (err, car) {
    res.render("Cars", { cars: car });
  });
});

// Contact Us
router.get("/ContactUs", forwardAuthenticated, (req, res) =>
  res.render("ContactUs")
);

// Dealership
router.get("/Dealership", (req, res) => {
  Dealerships.find(function (err, dealership) {
    res.render("Dealership", { dealerships: dealership });
  });
});

// test
router.get("/test", (req, res) => {
  // let car_list = [{ name: "a" }, { name: "b" }];
  // const collection = db.collection("cars");

  Cars.find(function (err, car) {
    //assert.equal(err, null);
    //console.log(car_list)

    res.render("test", { cars: car });
  });
});

// Dashboard
router.get("/dashboard", ensureAuthenticated, (req, res) =>
  res.render("dashboard", {
    user: req.user,
  })
);

module.exports = router;

```
## /config/passport.js
```nodejs
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');

// Load User model
const User = require('../models/User');

module.exports = function(passport) {
  passport.use(
    new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
      // Match user
      User.findOne({
        email: email
      }).then(user => {
        if (!user) {
          return done(null, false, { message: 'That email is not registered' });
        }

        // Match password
        bcrypt.compare(password, user.password, (err, isMatch) => {
          if (err) throw err;
          if (isMatch) {
            return done(null, user);
          } else {
            return done(null, false, { message: 'Password incorrect' });
          }
        });
      });
    })
  );

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });
};

```
## /config/keys.js
```nodejs
dbPassword = 'mongodb+srv://ngoj01:'+ encodeURIComponent('u3sXtk2X6G9OWMFN') + '@aaj.0ejs5.mongodb.net/aaj?retryWrites=true';

module.exports = {
    mongoURI: dbPassword
};

```
## /config/auth.js
```nodejs
module.exports = {
  ensureAuthenticated: function(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    req.flash('error_msg', 'Please log in to view that resource');
    res.redirect('/users/login');
  },
  forwardAuthenticated: function(req, res, next) {
    if (!req.isAuthenticated()) {
      return next();
    }
    res.redirect('/dashboard');      
  }
};

```
## /models/User.js
```nodejs
const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;

```
## /models/Cars.js
```nodejs
const mongoose = require("mongoose");

const carsSchema = new mongoose.Schema({
  make: {
    type: String,
  },
  model: {
    type: String,
  },
  color: {
    type: String,
  },
  year: {
    type: String,
  },
  price: {
    type: String,
  },
});

const cars = mongoose.model("cars", carsSchema);

module.exports = cars;

```
## /models/Dealerships.js
```nodejs
const mongoose = require("mongoose");

const dealershipsSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  address: {
    type: String,
  },
  city: {
    type: String,
  },
  state: {
    type: String,
  },
  zip: {
    type: String,
  },
  phone: {
    type: String,
  },
});

const dealerships = mongoose.model("dealerships", dealershipsSchema);

module.exports = dealerships;

```
