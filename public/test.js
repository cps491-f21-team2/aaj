function initMap() {
  // Create the map.
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 7,
    center: { lat: 52.632469, lng: -1.689423 },
    styles: mapStyle,
  });

  // Load the stores GeoJSON onto the map.
  map.data.loadGeoJson("stores.json", { idPropertyName: "storeid" });

  // Define the custom marker icons, using the store's "category".
  map.data.setStyle((feature) => {
    return {
      icon: {
        url: `img/icon_${feature.getProperty("category")}.png`,
        scaledSize: new google.maps.Size(64, 64),
      },
    };
  });

  const apiKey = "YOUR_API_KEY";
  const infoWindow = new google.maps.InfoWindow();

  // Show the information for a store when its marker is clicked.
  map.data.addListener("click", (event) => {
    const category = event.feature.getProperty("category");
    const name = event.feature.getProperty("name");
    const description = event.feature.getProperty("description");
    const hours = event.feature.getProperty("hours");
    const phone = event.feature.getProperty("phone");
    const position = event.feature.getGeometry().get();
    const content = sanitizeHTML`
        <img style="float:left; width:200px; margin-top:30px" src="img/logo_${category}.png">
        <div style="margin-left:220px; margin-bottom:20px;">
          <h2>${name}</h2><p>${description}</p>
          <p><b>Open:</b> ${hours}<br/><b>Phone:</b> ${phone}</p>
          <p><img src="https://maps.googleapis.com/maps/api/streetview?size=350x120&location=${position.lat()},${position.lng()}&key=${apiKey}"></p>
        </div>
        `;

    infoWindow.setContent(content);
    infoWindow.setPosition(position);
    infoWindow.setOptions({ pixelOffset: new google.maps.Size(0, -30) });
    infoWindow.open(map);
  });

  // Build and add the search bar
  const card = document.createElement("div");
  const titleBar = document.createElement("div");
  const title = document.createElement("div");
  const container = document.createElement("div");
  const input = document.createElement("input");
  const options = {
    types: ["address"],
    componentRestrictions: { country: "gb" },
  };

  card.setAttribute("id", "pac-card");
  title.setAttribute("id", "title");
  title.textContent = "Find the nearest store";
  titleBar.appendChild(title);
  container.setAttribute("id", "pac-container");
  input.setAttribute("id", "pac-input");
  input.setAttribute("type", "text");
  input.setAttribute("placeholder", "Enter an address");
  container.appendChild(input);
  card.appendChild(titleBar);
  card.appendChild(container);
  map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

  // Make the search bar into a Places Autocomplete search bar and select
  // which detail fields should be returned about the place that
  // the user selects from the suggestions.
  const autocomplete = new google.maps.places.Autocomplete(input, options);

  autocomplete.setFields(["address_components", "geometry", "name"]);

  // Set the origin point when the user selects an address
  const originMarker = new google.maps.Marker({ map: map });
  originMarker.setVisible(false);
  let originLocation = map.getCenter();

  autocomplete.addListener("place_changed", async () => {
    originMarker.setVisible(false);
    originLocation = map.getCenter();
    const place = autocomplete.getPlace();

    if (!place.geometry) {
      // User entered the name of a Place that was not suggested and
      // pressed the Enter key, or the Place Details request failed.
      window.alert("No address available for input: '" + place.name + "'");
      return;
    }

    // Recenter the map to the selected address
    originLocation = place.geometry.location;
    map.setCenter(originLocation);
    map.setZoom(9);
    console.log(place);

    originMarker.setPosition(originLocation);
    originMarker.setVisible(true);

    // Use the selected address as the origin to calculate distances
    // to each of the store locations
    const rankedStores = await calculateDistances(map.data, originLocation);
    showStoresList(map.data, rankedStores);

    return;
  });
}
