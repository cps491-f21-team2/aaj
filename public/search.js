// Shows the zip code
window.addEventListener("load", () => {
  const params = new URL(document.location).searchParams;
  const zip = params.get("zip");

  document.getElementById("result").innerHTML = zip;
});

function search() {
  alert("Hello World");
  const zip = params.get("zip");
}

function sanitizeHTML(strings) {
  const entities = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': "&quot;",
    "'": "&#39;",
  };
  let result = strings[0];
  for (let i = 1; i < arguments.length; i++) {
    result += String(arguments[i]).replace(/[&<>'"]/g, (char) => {
      return entities[char];
    });
    result += strings[i];
  }
  return result;
}

// Load Google Maps
function initMap() {
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 12,
    center: { lat: 39.7395, lng: -84.1788 },
  });

  const params = new URL(document.location).searchParams;
  const zip = params.get("zip");

  const apiKey = "AIzaSyB4ptHjW_Rvz4i-Ou0nwOk-yytCPSicGaU";
  const infoWindow = new google.maps.InfoWindow();

  // Load Dealerships Locations
  map.data.loadGeoJson("dealerships.json", { idPropertyName: "storeid" });

  // Show the information for a store when its marker is clicked.
  map.data.addListener("click", (event) => {
    const name = event.feature.getProperty("name");
    const address = event.feature.getProperty("address");
    const phone = event.feature.getProperty("phone");
    const position = event.feature.getGeometry().get();
    const content = sanitizeHTML`
      <img style="float:left; width:100px; margin-top:30px">
      <div style="margin-left:150px; margin-bottom:20px;">
        <h2>${name}</h2>
        <p> <b>Address: </b>${address}</p>
        <p> <b>Phone: </b> ${phone}</p>
        <p><img src="https://maps.googleapis.com/maps/api/streetview?size=350x120&location=${position.lat()},${position.lng()}&key=${apiKey}"></p>
      </div>
      `;

    infoWindow.setContent(content);
    infoWindow.setPosition(position);
    infoWindow.setOptions({ pixelOffset: new google.maps.Size(0, -30) });
    infoWindow.open(map);

    originLocation =
      "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyB4ptHjW_Rvz4i-Ou0nwOk-yytCPSicGaU&components=postal_code:45424";
    //const rankedStores = await calculateDistances(map.data, originLocation);
    // showStoresList(map.data, rankedStores);
  });
}

/**
 * Use Distance Matrix API to calculate distance from origin to each store.
 * @param {google.maps.Data} data The geospatial data object layer for the map
 * @param {google.maps.LatLng} origin Geographical coordinates in latitude
 * and longitude
 * @return {Promise<object[]>} n Promise fulfilled by an array of objects with
 * a distanceText, distanceVal, and storeid property, sorted ascending
 * by distanceVal.
 */
async function calculateDistances(data, origin) {
  const stores = [];
  const destinations = [];

  // Build parallel arrays for the store IDs and destinations
  data.forEach((store) => {
    const storeNum = store.getProperty("storeid");
    const storeLoc = store.getGeometry().get();

    stores.push(storeNum);
    destinations.push(storeLoc);
  });

  // Retrieve the distances of each store from the origin
  // The returned list will be in the same order as the destinations list
  const service = new google.maps.DistanceMatrixService();
  const getDistanceMatrix = (service, parameters) =>
    new Promise((resolve, reject) => {
      service.getDistanceMatrix(parameters, (response, status) => {
        if (status != google.maps.DistanceMatrixStatus.OK) {
          reject(response);
        } else {
          const distances = [];
          const results = response.rows[0].elements;
          for (let j = 0; j < results.length; j++) {
            const element = results[j];
            const distanceText = element.distance.text;
            const distanceVal = element.distance.value;
            const distanceObject = {
              storeid: stores[j],
              distanceText: distanceText,
              distanceVal: distanceVal,
            };
            distances.push(distanceObject);
          }

          resolve(distances);
        }
      });
    });

  const distancesList = await getDistanceMatrix(service, {
    origins: [origin],
    destinations: destinations,
    travelMode: "DRIVING",
    unitSystem: google.maps.UnitSystem.METRIC,
  });

  distancesList.sort((first, second) => {
    return first.distanceVal - second.distanceVal;
  });

  return distancesList;
}

// Displays the list of dealerships
function showStoresList(data, dealerships) {
  if (dealerships.length == 0) {
    console.log("empty stores");
    return;
  }

  let panel = document.createElement("div");
  // If the panel already exists, use it. Else, create it and add to the page.
  if (document.getElementById("panel")) {
    panel = document.getElementById("panel");
    // If panel is already open, close it
    if (panel.classList.contains("open")) {
      panel.classList.remove("open");
    }
  } else {
    panel.setAttribute("id", "panel");
    const body = document.body;
    body.insertBefore(panel, body.childNodes[0]);
  }

  // Clear the previous details
  while (panel.lastChild) {
    panel.removeChild(panel.lastChild);
  }

  dealerships.forEach((store) => {
    // Add store details with text formatting
    const name = document.createElement("p");
    name.classList.add("place");
    const currentStore = data.getFeatureById(store.storeid);
    name.textContent = currentStore.getProperty("name");
    panel.appendChild(name);
    const distanceText = document.createElement("p");
    distanceText.classList.add("distanceText");
    distanceText.textContent = store.distanceText;
    panel.appendChild(distanceText);
  });

  // Open the panel
  panel.classList.add("open");

  return;
}

const getResultIndex = function (elem) {
  return parseInt(elem.getAttribute("data-location-index"));
};

locator.renderResultsList = function () {
  let locations = locator.locations.slice();
  for (let i = 0; i < locations.length; i++) {
    locations[i].index = i;
  }
  if (locator.searchLocation) {
    sectionNameEl.textContent = "Nearest locations (" + locations.length + ")";
    locations.sort(function (a, b) {
      return getLocationDistance(a) - getLocationDistance(b);
    });
  } else {
    sectionNameEl.textContent = `All locations (${locations.length})`;
  }
  const resultItemContext = { locations: locations };
  resultsContainerEl.innerHTML = itemsTemplate(resultItemContext);
  for (let item of resultsContainerEl.children) {
    const resultIndex = getResultIndex(item);
    if (resultIndex === locator.selectedLocationIdx) {
      item.classList.add("selected");
    }

    const resultSelectionHandler = function () {
      selectResultItem(resultIndex, true, false);
    };

    // Clicking anywhere on the item selects this location.
    // Additionally, create a button element to make this behavior
    // accessible under tab navigation.
    item.addEventListener("click", resultSelectionHandler);
    item
      .querySelector(".select-location")
      .addEventListener("click", function (e) {
        resultSelectionHandler();
        e.stopPropagation();
      });
  }
};
function ajaxSearch() {
  var input = $("#search").val();
  //alert("Debug: ajaxSearch()->input=" + input);
  const xhttp = new XMLHttpRequest();
  xhttp.onload = function () {
    document.getElementById("response").innerHTML = this.responseText;
  };
  xhttp.open("GET", "/test/ziplocate?zip=" + input);
  xhttp.send();

  /*
  $.ajax("/Dealership/ziplocate?zip=" + input, function (result) {
    //alert("got ajax result")
    global_result = result;
    $("#response").html("Response from  server" + JSON.stringify(result));
    $("#response").html(displayData(result));
  }); //get
  $("#data").val("");
  */
}

/* function displayData(jsondata) {
  if (jsondata.length == 0) return "No results";
  var htmltable = "<table><tr>" + "<th>Dealership:</th>" + "<th>ID: </th>";

  for (var i = 0; i < jsondata.length; i++) {
    htmltable +=
      "<tr>" +
      "<td>" +
      jsondata[i].name +
      "</td>" +
      "<td>" +
      jsondata[i]._id +
      "</td>" +
      "<td>" +
      jsondata[i].Startyear +
      "</td>";
  }
}*/
